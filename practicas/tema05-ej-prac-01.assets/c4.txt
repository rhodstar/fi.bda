sys@rfpbda2> @b.sql
Connected.

                                                                                                                                                                                                        

T02_DISPATCHER_CONFIG

        ID DISPATCHERS CONNECTIONS   SESSIONS SERVICE                                                                                                                                                   
---------- ----------- ----------- ---------- --------------------                                                                                                                                      
         1           2        1022       1022 rfpbda2.fi.unam                                                                                                                                           

T03_DISPATCHER

        ID NAME NETWORK                        STATUS             MESSAGES MESSAGES_MB CIRCUITS_CREATED   IDLE_MIN                                                                                      
---------- ---- ------------------------------ ---------------- ---------- ----------- ---------------- ----------                                                                                      
         1 D000 (ADDRESS=(PROTOCOL=tcp)(HOST=p WAIT                    401         .06                1 62.1047222                                                                                      
                c-rfp.fi.unam)(PORT=29945))                                                                                                                                                             
                                                                                                                                                                                                        
         1 D001 (ADDRESS=(PROTOCOL=tcp)(HOST=p WAIT                    334         .04                1 62.1047222                                                                                      
                c-rfp.fi.unam)(PORT=26861))                                                                                                                                                             
                                                                                                                                                                                                        

T04_SHARED_SERVER

        ID NAME STATUS             MESSAGES MESSAGES_MB   REQUESTS   IDLE_MIN   BUSY_MIN                                                                                                                
---------- ---- ---------------- ---------- ----------- ---------- ---------- ----------                                                                                                                
         1 S000 WAIT(COMMON)              0           0          0 62.1086111          0                                                                                                                
         1 S001 WAIT(COMMON)              0           0          0 62.1083333          0                                                                                                                
         1 S002 WAIT(COMMON)              0           0          0 62.1080556          0                                                                                                                
         1 S003 EXEC                    737         .22        116 58.6880556 3.41944444                                                                                                                

T05_QUEUE

        ID     QUEUED       WAIT     TOTALQ                                                                                                                                                             
---------- ---------- ---------- ----------                                                                                                                                                             
         1          0          0         58                                                                                                                                                             
         1          0          0         58                                                                                                                                                             
         1          0          1        202                                                                                                                                                             
         1          0          0        167                                                                                                                                                             

T06_VIRTUAL_CIRCUIT

        ID CIRCUIT          NAME SERVER           STATUS           QUEUE                                                                                                                                
---------- ---------------- ---- ---------------- ---------------- ----------------                                                                                                                     
         1 0000000070034028 D000 00000000662A6FA8 NORMAL           SERVER                                                                                                                               
         1 0000000070034028 D001 00000000662A6FA8 NORMAL           SERVER                                                                                                                               

T07_SESSION_INFO_CONTEXT

HOST                 OS_USER              USER_ID              SESSION_ID                                                                                                                               
-------------------- -------------------- -------------------- --------------------                                                                                                                     
pc-rfp.fi.unam       rodrigo              0                    4294967295                                                                                                                               

T08_SESSION_INFO_VIEW

SESSION_ID PROCESS_ADDRESS  BD_USERNAME          SESSION_ CLIENT_PORT OS_CLIENT_PROCESS_ID     CLIENT_PROGRAM                                                                                           
---------- ---------------- -------------------- -------- ----------- ------------------------ ------------------------------------------------                                                         
       102 00000000662C58C8 SYS                  ACTIVE             0 6547                     sqlplus@pc-rfp.fi.unam (TNS V1-V3)                                                                       

T09_PROCESS_INFO

SOSID                    PNAME B TRACEFILE                                                                                                                                                              
------------------------ ----- - --------------------------------------------------------------------------------                                                                                       
6641                             /u01/app/oracle/diag/rdbms/rfpbda2/rfpbda2/trace/rfpbda2_ora_6641.trc                                                                                                  

T10_BACKGROUND_PROCESS

ADDR             SOSID                    PNAME OS_USERNAME     B                                                                                                                                       
---------------- ------------------------ ----- --------------- -                                                                                                                                       
0000000066280C40 3858                     PMON  rodrigo         1                                                                                                                                       
0000000066281DB8 3860                     CLMN  rodrigo         1                                                                                                                                       
0000000066282F30 3862                     PSP0  rodrigo         1                                                                                                                                       
00000000662840A8 3865                     VKTM  rodrigo         1                                                                                                                                       
0000000066285220 3869                     GEN0  rodrigo         1                                                                                                                                       
0000000066286398 3871                     MMAN  rodrigo         1                                                                                                                                       
0000000066287510 3891                     DIA0  rodrigo         1                                                                                                                                       
0000000066288688 3875_3876                GEN1  rodrigo         1                                                                                                                                       
0000000066289800 3875_3875                SCMN  rodrigo         1                                                                                                                                       
000000006628A978 3878                     DIAG  rodrigo         1                                                                                                                                       
000000006628BAF0 3880_3881                OFSD  rodrigo         1                                                                                                                                       

ADDR             SOSID                    PNAME OS_USERNAME     B                                                                                                                                       
---------------- ------------------------ ----- --------------- -                                                                                                                                       
000000006628CC68 3880_3880                SCMN  rodrigo         1                                                                                                                                       
000000006628DDE0 3883                     DBRM  rodrigo         1                                                                                                                                       
000000006628EF58 3885                     VKRM  rodrigo         1                                                                                                                                       
00000000662900D0 3887                     SVCB  rodrigo         1                                                                                                                                       
0000000066291248 3889                     PMAN  rodrigo         1                                                                                                                                       
00000000662923C0 3894                     DBW0  rodrigo         1                                                                                                                                       
0000000066293538 3896                     LGWR  rodrigo         1                                                                                                                                       
00000000662946B0 3898                     CKPT  rodrigo         1                                                                                                                                       
0000000066295828 3900                     LG00  rodrigo         1                                                                                                                                       
00000000662969A0 3902                     SMON  rodrigo         1                                                                                                                                       
0000000066297B18 3904                     LG01  rodrigo         1                                                                                                                                       

ADDR             SOSID                    PNAME OS_USERNAME     B                                                                                                                                       
---------------- ------------------------ ----- --------------- -                                                                                                                                       
0000000066298C90 3906                     SMCO  rodrigo         1                                                                                                                                       
0000000066299E08 3908                     RECO  rodrigo         1                                                                                                                                       
000000006629AF80 3910                     W000  rodrigo         1                                                                                                                                       
000000006629C0F8 3912                     LREG  rodrigo         1                                                                                                                                       
000000006629D270 3914                     W001  rodrigo         1                                                                                                                                       
000000006629E3E8 3916                     PXMN  rodrigo         1                                                                                                                                       
00000000662A06D8 3920                     MMON  rodrigo         1                                                                                                                                       
00000000662A1850 3922                     MMNL  rodrigo         1                                                                                                                                       
00000000662A8120 3936                     TMON  rodrigo         1                                                                                                                                       
00000000662A9298 4756                     W005  rodrigo         1                                                                                                                                       
00000000662AA410 3939                     M000  rodrigo         1                                                                                                                                       

ADDR             SOSID                    PNAME OS_USERNAME     B                                                                                                                                       
---------------- ------------------------ ----- --------------- -                                                                                                                                       
00000000662AE9F0 3982                     TT00  rodrigo         1                                                                                                                                       
00000000662AFB68 3984                     TT01  rodrigo         1                                                                                                                                       
00000000662B0CE0 3986                     TT02  rodrigo         1                                                                                                                                       
00000000662B1E58 3990                     W002  rodrigo         1                                                                                                                                       
00000000662B2FD0 3992                     AQPC  rodrigo         1                                                                                                                                       
00000000662B4148 3998                     CJQ0  rodrigo         1                                                                                                                                       
00000000662B52C0 4013                     W003  rodrigo         1                                                                                                                                       
00000000662BBB90 4065                     QM02  rodrigo         1                                                                                                                                       
00000000662BCD08 4029                     M001  rodrigo         1                                                                                                                                       
00000000662BDE80 4031                     M002  rodrigo         1                                                                                                                                       
00000000662BEFF8 4033                     M003  rodrigo         1                                                                                                                                       

ADDR             SOSID                    PNAME OS_USERNAME     B                                                                                                                                       
---------------- ------------------------ ----- --------------- -                                                                                                                                       
00000000662C0170 4035                     M004  rodrigo         1                                                                                                                                       
00000000662C12E8 4760                     W006  rodrigo         1                                                                                                                                       
00000000662C2460 4054                     W004  rodrigo         1                                                                                                                                       
00000000662C35D8 4767                     W007  rodrigo         1                                                                                                                                       
00000000662C4750 4071                     Q003  rodrigo         1                                                                                                                                       
00000000662C8D30 4109                     Q008  rodrigo         1                                                                                                                                       

50 rows selected.

T11_FOREGROUND_PROCESS

ADDR             SOSID                    PNAME BD_USERNAME          OS_USERNAME     B                                                                                                                  
---------------- ------------------------ ----- -------------------- --------------- -                                                                                                                  
00000000662A5E30 3932                     S002                       rodrigo                                                                                                                            
000000006629F560 3924                     D000                       rodrigo                                                                                                                            
00000000662A29C8 3926                     D001                       rodrigo                                                                                                                            
00000000662A3B40 3928                     S000                       rodrigo                                                                                                                            
00000000662B75B0 4004                     P004                       rodrigo                                                                                                                            
00000000662BAA18 4010                     P007                       rodrigo                                                                                                                            
00000000662AD878 3979                     P002                       rodrigo                                                                                                                            
00000000662A4CB8 3930                     S001                       rodrigo                                                                                                                            
00000000662AB588 3975                     P000                       rodrigo                                                                                                                            
00000000662B98A0 4008                     P006                       rodrigo                                                                                                                            
00000000662A6FA8 3934                     S003                       rodrigo                                                                                                                            

ADDR             SOSID                    PNAME BD_USERNAME          OS_USERNAME     B                                                                                                                  
---------------- ------------------------ ----- -------------------- --------------- -                                                                                                                  
00000000662AC700 3977                     P001                       rodrigo                                                                                                                            
00000000662B8728 4006                     P005                       rodrigo                                                                                                                            
00000000662B6438 4002                     P003                       rodrigo                                                                                                                            
000000006627FAC8                                                                                                                                                                                        
00000000662C58C8 6641                                                rodrigo                                                                                                                            

16 rows selected.

sys@rfpbda2> spool off
